#Jarod Hoeflich
#September 15, 2015
#Color Experiment

def decreaseRed(img):
  for pixel in getPixels(img):
    redVal = 0
    setRed(pixel,redVal)

file = '/Users/jhoeflich2017/Documents/week3/image.jpg'
picture = makePicture(file)
show(picture)

decreaseRed(picture)

repaint(picture)