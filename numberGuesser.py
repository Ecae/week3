#Jarod Hoeflich
#September 14, 2015
#Number Guesser Game
import sys
import random
target = random.randint(1,100)

guess = 0

while(guess is not 5):
	x = int(input('Pick a number between one and one hundred'))
	if x > target and x < 101:
		print('Too high, try again')
		guess += 1
	if x < target and x > 0:
		print('Too low, try again')
		guess += 1
	if x == target:
		print('You are correct, good job!')
		sys.exit()